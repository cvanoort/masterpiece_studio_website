# Masterpiece Jewelry Studio Website

## Adding new products
Adding new products to the site is easy, just follow these steps:

- Copy and rename one of the existing product files in the `products/` folder.
  - The products files use the [JSON format](https://www.json.org/json-en.html), which is human and 
  machine readable.
- Update the values in the file such as the price, item description, tags, etc.
- Place an image in the products folder.
  - Ensure the image location in the JSON file points to this image.
  - Currently the product images all have a resolution of 512x384, this stikes a balance between quality an page load time.
    - Higher quality images may be used, but please ensure that they have the same aspect ratio (4:3).
  - Images are compressed using `jpegoptim -m 80 {image name}` to reduce their file size (and thus page load times).
- Create a thumbnail of the product image that is approximately 20x20 and heavily compressed. 
Name it `{image_name}_tiny.jpg`.
  - `make_load_images.sh` can create / update thumbnail images for all of the products at once.
  - This thumbnail is loaded first, then the higher quality image is loaded on demand afterwards.
  This improves page load times.
- When you're happy with the products that you've added / removed / updated, you'll need to commit and push the changes to the GitLab repository.
- Once the repository has been updated, the last step is to update the deployed website through the hosting service.

The next time the page is refreshed the new product will be included on the products page.
Note that depending on the caching policy of the web server you may need to perform a hard refresh 
or clear your cache to see the changes.

## Development Tools
### Setup
To update the webiste, we'll need a few tools.
If you're on Windows, start by installing [Windows Subsystem for Linux (WSL)](https://learn.microsoft.com/en-us/windows/wsl/install).
Note that WSL installs Ubuntu by default, which comes with Git.
Next, ensure your system is up to date.
For Ubuntu users (native or via WSL), run `sudo apt update && sudo apt upgrade`.
Next, install PHP so that we can view the changes that we're making to the website before deploying them.
For Ubuntu users (native or via WSL), run `sudo apt install php`.
Next, install the image editing tools needed to manage product images, `jpegoptim` and `imagemagick`.
For Ubuntu users (native or via WSL), run `sudo apt install jpegoptim imagemagick`.

Finally, if you haven't already cloned the repository, move to the desired folder and run `git clone https://gitlab.com/cvanoort/masterpiece_studio_website.git`.

### Usage
Once you have all of the tools installed, then you're ready to update the website.
To check out the changes that you're making:
- In your terminal, move to the top level of the repository: `cd masterpiece_studio_website`
- Start a PHP server: `php -S 127.0.0.1:8000`
- View the website: paste `127.0.0.1:8000` into your browser


## Resources
- [Start Bootstrap - Business Casual Template](https://startbootstrap.com/template-overviews/business-casual/)
  - [Business Casual](http://startbootstrap.com/template-overviews/business-casual/) is a multipurpose website theme for [Bootstrap](http://getbootstrap.com/) created by [Start Bootstrap](http://startbootstrap.com/). This theme features a landing page, about page, blog page, and a contact page along with various custom styles and components.
- [Lux Bootstrap Theme by Bootswatch](https://bootswatch.com/lux/) provides most of the color scheme.
- [progressive-image.js](https://github.com/craigbuckler/progressive-image.js) facilitates fast page loads.
- `make_load_images.sh` requires [imagemagick](https://imagemagick.org/index.php) and [jpegoptim](https://github.com/tjko/jpegoptim) 
