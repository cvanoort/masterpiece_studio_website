#!/usr/bin/bash
# Creates tiny versions of all images in the provided directory, and each subdirectory.
# These images will be approximately 20x20 and heavily compressed.
# Created images should be convenient to use for progressive loading of images on websites.
# Uses imagemagick (convert) and jpegoptim.
# Ensure that these are installed before use.
# See https://imagemagick.org/index.php and https://github.com/tjko/jpegoptim for more details

dir=${1}

rm $dir/*_tiny.jpg
rm $dir/**/*_tiny.jpg

for f in $dir/*.jpg ; do
    convert "$f" -resize 20x20  "${f%.jpg}_tiny.jpg"
    jpegoptim -m 5 "${f%.jpg}_tiny.jpg"
done

for f in $dir/**/*.jpg ; do
    convert "$f" -resize 20x20  "${f%.jpg}_tiny.jpg"
    jpegoptim -m 5 "${f%.jpg}_tiny.jpg"
done