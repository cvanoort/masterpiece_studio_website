import argparse
import json
from pathlib import Path

import pandas as pd


def get_parser():
    parser = argparse.ArgumentParser(
        description="Automates the management of products displayed on the Masterpiece Jewelry Studio Website.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "--action",
        choices=['search', 'delete', 'update', 'add_date'],
        help="Selects an action to perform on the products.",
    )

    parser.add_argument(
        "--type",
        choices=['ring', 'ear', 'watch', 'necklace', 'bracelet'],
        help="Indicates a jewelry type to be targeted."
    )

    parser.add_argument(
        "--price",
        nargs='*',
        help="Selects products with a specific price or that fall within a price range."
    )

    parser.add_argument(
        "--date_added",
        nargs='*',
        help="Zero, one, or two dates used to select products. "
             "Zero selects products that do not have a date-added attribute, "
             "one date selects products whose date-added attribute exactly equals the provided date,"
             "two dates selects products whose date-added attribute falls between the provided dates."
    )

    parser.add_argument(
        "--tags",
        nargs='*',
        help="Zero or more tags that should be present on selected products."
    )
    return parser


def load_products():
    products = []
    for p in sorted(Path('products').glob('*.json')):
        with open(p, 'r') as f:
            products.append((p, json.load(f)))
    return products


def filter_products(products, type, price, date_added, tags):
    print(f'{len(products)} products before filtering.')

    if type:
        products = [p for p in products if p[1]['type'] == type]

    print(f'{len(products)} products after filtering by jewelry type {type}.')

    if price:
        if len(price) == 1:
            price = price[0]

            try:
                price = float(price)
            except ValueError:
                pass

            products = [
                p for p in products
                if p[1]['price'] == price
            ]
        elif len(price) == 2:
            low, high = [float(x) for x in price]
            products = [
                p for p in products
                if isinstance(p[1]['price'], float) and (low <= p[1]['price'] <= high)
            ]

    print(f'{len(products)} products after filtering by price {price}')

    if date_added:
        if len(date_added) == 0:
            products = [
                p for p in products
                if not p[1]['date-added']
            ]
        elif len(date_added) == 1:
            date_added = date_added[0]
            products = [
                p for p in products
                if p[1]['date-added'] == date_added
            ]
        elif len(date_added) == 2:
            low, high = (pd.Timestamp(d) for d in date_added)
            products = [
                p for p in products
                if low <= pd.Timestamp(p[1]['date-added']) <= high
            ]

    print(f'{len(products)} products after filtering by date-added {date_added}')

    if tags:
        products = [p for p in products if set(p[1]['tags']) >= set(tags)]

    print(f'{len(products)} products after filtering by tags {tags}.\n')

    return products


def main(action, type, price, date_added, tags):
    products = filter_products(
        load_products(),
        type,
        price,
        date_added,
        tags,
    )

    if action == 'search':
        for (f, p) in products:
            print(f)
    elif action == 'delete':
        print(f'Deleting {len(products)} products.')
        for (f, p) in products:
            f.unlink()
            Path(p['img']).unlink()
    elif action == 'update':
        pass
    elif action == 'add_date':
        for (f, p) in products:
            if not p['date-added']:
                p[date_added] = str(pd.Timestamp.now().date())
                json.dump(p, f)


if __name__ == '__main__':
    args = vars(get_parser().parse_args())
    main(**args)
